# IOC for TS2 SmartPirani vacuum gauge

## Used modules

*   [vac_ctrl_sens4_vpm7](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_sens4_vpm7)


## Controlled devices

*   TS2-010CRM:Vac-VGP-012
