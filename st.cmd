#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_sens4_vpm7
#
require vac_ctrl_sens4_vpm7,1.1.0

#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, $(vac_ctrl_sens4_vpm7_DB))


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Vac-VGP-012
# Module: vac_ctrl_sens4_vpm7
#
iocshLoad($(vac_ctrl_sens4_vpm7_DIR)/vac_ctrl_sens4_vpm7_moxa.iocsh, "DEVICENAME = TS2-010CRM:Vac-VGP-012, IPADDR = moxa-vac-ts2, PORT = 4004")
